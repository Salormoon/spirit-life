"""Симуляция жизни духов.

Это простая симуляция искуственно жизни с некоторыми правилами.

- Единица жизни - дух.
- Дух жиёт 1 цикл.
- Каждый дух оставляет до 4х потомков.
- Более сильные потмки поглащают слабых.
- Несколько потомков рядом объединяются в одного.

Author: Milinuri Nirvalen
Version: 0.5-21
"""

from typing import NamedTuple
from pathlib import Path
from shutil import get_terminal_size
import argparse
import re

from PIL import Image
import numpy
from loguru import logger

from icecream import ic


class Color(NamedTuple):
    """Описание цвета в цветовом пространстве RGB.

    Используется при описании цвета в палитре.

    :param red: Яркость красного цвета. От 0 до 255.
    :type red: int
    :param green: Яркость зелёного цвета. От 0 до 255.
    :type green: int
    :param blue: Яркость синего цвета. От 0 до 255.
    :type blue: int
    """

    red: int
    green: int
    blue: int

class Pixel(NamedTuple):
    """Представление пикселя в жвумерном массиве.

    Пиксель - наименьшая единица растрового изображения.
    Наименьшая точка, с которой мы работаем.
    Данный класс описывает пиксель с точки зрения двумерного массива.
    Вместо цаета мы храним `мощность".
    После преобразования мощность будет переведена в цвет согласно палитре.

    :param x: Положение пиксеоя по горизонтали. От 0 до размера холста.
    :type x: int
    :param y: Положение пикселя по вертикали. От 0 до размера холста.
    :type y: int
    :param value: Энергетическая мощность в данном пикселе. От 0 до 6.
    :type value: int
    """

    x: int
    y: int
    value: int

    def update(self, value: int):
        """Заменяет текущее значение пикселя.

        Очень часто нужно заменить значение пикселя, не зименяя его
        координат.
        Данная функция используется повсеместно, где нужно заменить
        значение значение текуего пикселя новым.

        :param value: Новое значение пикселя.
        :type value: int
        :return: Пиксель со старыми координатами, но новым значением.
        :rtype: Pixel
        """
        return Pixel(self.x, self.y, value)

class Neighobors(NamedTuple):
    """Описывает соседей конкретного пикселя.

    Описание ближайших соседей используется при рассеивании энергии.

    :param up: Сосед сверху или None, если его нет.
    :type up: Pixel | None
    :paran right: Сосед справа или None, если его нет.
    :type right: Pixel | None
    :param down: Сосед снизу или None, если его нет.
    :type down: Pixel | None
    :param left: Сосед слева или None, если его нет.
    :type left: Pixel | None
    """

    up: Pixel | None
    right: Pixel | None
    down: Pixel | None
    left: Pixel | None

    def get_charged(self) -> int:
        """Возвращает количествл заряженных соседей.

        Сосед считается зарчженнвм, если ешо энергия больше 0 или он None.
        None считает за заряженного соседа для прилипания к краям холста.
        используеься при нормализации энергии.

        :return: Заряженные соседи. от 0 до 4.
        :rtype: int
        """
        res = 0
        for x in self:
            if x is None or x.value > 0:
                res += 1
        return res

class Destination(NamedTuple):
    """Описаывает расстояние до ближайшего пикселя в массиве.

    Используеся при относительном поиске соседей.

    :param x: Расстоние до пикселя по горизонтали.
    :type x: int
    :param y: расстояние до пикселя по вертикали.
    :type y: int
    :param index: Укзаание индекса ближайшего соседа
    :type index: int
    """
    x: int
    y: int
    index: int

    @property
    def min_dest(self) -> int:
        return self.x if self.x < self.y else self.y

    def in_radius(self, r: int) -> bool:
        return self.x <= r and self.y <= r


# Паоитра цветов
# ==============
#
# Используется для преобразования пиксельной карты в цветовую.
# Каждая мощность пикселя заменяется на соответствующий цвет палитры.
#
# В будущих версиях функционал палитры будет расширен

palette = [
    Color(15, 17, 26),
    Color(62, 45, 45),
    Color(110, 76, 73),
    Color(148, 114, 93),
    Color(191, 161, 122),
    Color(221, 207, 148),
    Color(222, 215, 189),
]


# Методы для отображения
# ======================

def rc(text: str) -> str:
    """Очищает текст от цветовых кодов."""
    return re.sub(r"\033\[[0-9]*m", "", text)

def progress_line(i: int, total: int, size: int | None = 50) -> str:
    """Возвращает строковую линию прогресса заданной длинны.

    Args:
        i (int): Текущее числовое значение.
        total (int): Максимальное Числовое значение.
        size (int, optional): Длинна линии прогресса.

    Returns:
        str: Тексовая линия прогресса.
    """
    bar_on = round(i/total*size)
    return f"\033[92m{'●'*bar_on}\033[90m{'●'*(size - bar_on)}\033[0m"


class ProgressBar:
    """Небольшой класс для создания итерируемого прогрессбаров.

    Args:
        total (int): Количество элементов прогрессбара."""

    def __init__(self, total: int):
        super(ProgressBar, self).__init__()
        self.total = total
        self._i = 0

    def progress_bar(self, text: str) -> None:
        """Выводит прогресс бар в терминал.

        Args:
            text (str): Комментарий к прогресс бару.
        """
        pline = progress_line(self._i, self.total)
        pr = round(self._i/self.total*100, 2)
        pb = f"{pline} [{self.total-self._i}] {pr}%: "
        ts = get_terminal_size()[0] - len(rc(pb))

        if len(text) <= ts:
            pb += f"{text}"
        else:
            tp = ts // 2
            pb = pb + text[:tp] + "..." + text[-tp+3:]

        if self._i < self.total:
            print(pb, end="\r")
        else:
            print(pb)

    def iter(self, text: str | None ="", clear_line: bool | None =True) -> None:
        self._i += 1
        self.progress_bar(text)

    def uniter(self, text: str | None ="") -> None:
        self._i -= 1
        self.progress_bar(text)


# Работа с пикселями
# ==================

def get_nerby_pixel(pix: Pixel, pixels: list[Pixel]) -> Destination:
    """Функция для относительного поиска ближайшего соседнего пикселя.

    Используется при работе с совмещение семян.

    :param pix: Начальные пиксель для поиска соседей.
    :type pix: Pixel
    :param pixels: Список соседей, среди которых искать ближайший.
    :type pix: list[Pixel]
    :return: Расстояние до ближайшего пикселя.
    :rtype: Destination
    """
    dest = Destination(100, 100, 0)

    for i, p in enumerate(pixels):
        dist_x = abs(pix.x - p.x)
        dist_y = abs(pix.y - p.y)

        if dist_x == 0 and dist_y == 0:
            continue

        if (dist_x+dist_y) < (dest.x+dest.y):
            dest = Destination(dist_x, dist_y, i)
    return dest

def get_pixels_in_radius(pix: Pixel, pixels: list[Pixel], r: int) -> list[Destination]:
    res = []

    for i, p in enumerate(pixels):
        dist_x = abs(pix.x - p.x)
        dist_y = abs(pix.y - p.y)

        if dist_x == 0 and dist_y == 0:
            continue

        if dist_x <= r and dist_y <= r:
            res.append(Destination(dist_x, dist_y, i))

    return res


class PixMap:
    """Представляет пиксельный холст и взаимодействие пиксеец с ним.

    Прежоставляет методы для раюоты с жвумерным массивом чисел.
    Каждое число в массиве является энергетическим запасом данного пикселя.
    Предоставляет методы для управления энергией.

    :param size: Размер квадратного холста.
    :type size: int
    :param start_seed: Пиксель пераого семечка существа.
    :type start_seed: Pixel | None
    """

    def __init__(self, size: int, start_seed: Pixel | None=None):
        self.size = size
        self.data = [[0 for _ in range(size)]  for _ in range(size)]

    def __len__(self) -> int:
        res = 0
        for y, l in enumerate(self.data):
            for x, v in enumerate(l):
                if v > 1:
                    res += 1
        return res

    def __iter__(self) -> Pixel:
        for y, l in enumerate(self.data):
            for x, v in enumerate(l):
                if v > 1:
                    yield Pixel(x, y, v)

    def colorize(self, palette: list[Color]) -> list[list[Color]]:
        """Раскрашивает пиксельную карту с помощью палитры.

        Используется при экспорте картины, например в изображение.

        :param palette: Пилитра цвето для раскраски холста.
        :type palette: list[Color]
        :return: Массив пикселей готового изображения.
        :rtype: list[list[Color]]
        """
        return [[palette[min(i, 6)] for i in r] for r in self.data]

    def clear(self) -> None:
        """Очищает осдержимое пиксельной карты."""
        self.data = [[0 for _ in range(size)]  for _ in range(size)]


    # Работа с каждым пикселем
    # ========================

    def get_pixel(self, x: int, y: int) -> Pixel | None:
        """Получает пиксель на координатам холста.

        Повсеместно используется при работе с холстом.
        Если указанные координаты выходят за пределы холста,
        возвращает None.

        :param x: Позиция по горизонтали. От 0 до размера холста.
        :type x: int
        :param y: Позиция по вертикали от 0 до размера холста.
        :type y: int
        :return: Экземпляр пикселя на холсте.
        :rtype: Pixel | None
        """
        if x < 0 or x > self.size-1 or y < 0 or y > self.size-1:
            return None
        return Pixel(x, y, self.data[y][x])

    def set_pixel(self, pix: Pixel) -> None:
        """Устанавливает новое значение пикселя на холста.

        Исползует координаты из переданного пикселя, чтобы установить
        новое значнеия для пикселя на холсте.

        :param pix: Экземпляр пикселя, который нужно установить.
        :type pix: Pixel
        """
        if pix.x < 0 or pix.x > self.size-1 or pix.y < 0 or pix.y > self.size-1:
            return
        self.data[pix.y][pix.x] = pix.value

    def get_neighbors(self, x: int, y: int) -> Neighobors:
        """Получение соседеей конкретного пикселя.

        :param x: Положение по горизонтали. От 0 до размера холста.
        :type x: int
        :param y: Положение по вертикали. От 0 до размера холста.
        :type y: int
        :return: Экземпляр соседей пикселя.
        :rtype: Neighobors
        """
        return Neighobors(
            up=self.get_pixel(x, y-1),
            right=self.get_pixel(x+1, y),
            down=self.get_pixel(x, y+1),
            left=self.get_pixel(x-1, y)
        )

    def search_pixels(self, pix: Pixel) -> list[Pixel]:
        """Более высокоуровневая функция поиска соседей.

        Она будет искать всех сосдей с подходям уровнем мозности
        в радиусе 2-х пикселей от целевого.
        Здесь переданная в пиксель мощность будет использоваться
        как минимальный порог для поиска пикселей.

        :param pix: Центральный пиксель, от которого начинвать поиск.
        :type pix: Pixel
        :return: Список всех найденных пикселей округе.
        :rtype: list[Pixel]
        """
        res = []
        for y in range(-2, 3):
            for x in range(-2, 3):
                if x == 0 and y == 0:
                    continue

                s_pix = self.get_pixel(pix.x+x, pix.y+y)
                if s_pix is not None and s_pix.value >= 2:
                    res.append(s_pix)
        return res


class SeedMap(PixMap):
    def __init__(self, size: int):
        super().__init__(size)

    def _process_seed(self, seed: Pixel) -> int:
        value = seed.value
        for nerby_pix in self.search_pixels(seed):
            if value > nerby_pix.value:
                self.set_pixel(nerby_pix.update(1))
            elif seed.value == nerby_pix.value:
                value += 1
                self.set_pixel(nerby_pix.update(1))
            else:
                self.set_pixel(nerby_pix.update(1))
        return value

    def get_seeds(self, pix: Pixel) -> None:
        """Получает до 4-х семян-наследников текущего существа.

        Используется при процессе симуляции, для продолжаения рода
        конкретного существа.

        :param pix: Пиксель начала отсчёта существа.
        :type pix: Pixel
        :return: Список пикселей полученных семян.
        :rtype: list[Pixel]
        """
        if pix.value <= 2:
            return

        potentia = 0

        # Up in bounders
        if pix.y-pix.value >= 0:
            seed = Pixel(pix.x, pix.y-pix.value, pix.value-1)
            value = self._process_seed(seed)
            self.set_pixel(seed.update(value))
        else:
            potentia += 1

        # Right in boumders
        if pix.x+pix.value < self.size:
            if potentia:
                seed = Pixel(pix.x+pix.value, pix.y, pix.value)
                potentia -= 1
            else:
                seed = Pixel(pix.x+pix.value, pix.y, pix.value-1)

            value = self._process_seed(seed)
            self.set_pixel(seed.update(value))
        else:
            potentia += 1

        # Down in bounders
        if pix.y+pix.value < self.size:
            if potentia:
                seed = Pixel(pix.x, pix.y+pix.value, pix.value)
                potentia -= 1
            else:
                seed = Pixel(pix.x, pix.y+pix.value, pix.value-1)

            value = self._process_seed(seed)
            self.set_pixel(seed.update(value))
        else:
            potentia += 1

        # Left in bouders
        if pix.x-pix.value >= 0:
            if potentia:
                seed = Pixel(pix.x-pix.value, pix.y, pix.value)
            else:
                seed = Pixel(pix.x-pix.value, pix.y, pix.value-1)

            value = self._process_seed(seed)
            self.set_pixel(seed.update(value))


class Canvas(PixMap):

    def __init__(self, size: int, start_seed: Pixel | None=None):
        super().__init__(size)
        self.seeds = SeedMap(size)
        self.spirits = []

        if start_seed is not None:
            self.seeds.set_pixel(start_seed)

    # Прообразования
    # ==============

    def normalize(self) -> None:
        """Нормализует распределние базовой энергии.

        Это позволяет сгладить неровности и позволить энергии
        растекаться подобно жидкости.

        Работает нормализация по тем правилам, что если у целевого
        пикселя с трёх сторон есть заряженный энергией сосед, то и
        целевой пиксель также становиться заряженным.

        Если же одна из сторое является границей, то она также
        считается заряженной, чтобы работало так называемое прилипание
        к границам холста.
        """
        logger.info("Normalize pixmap")
        for y, l in enumerate(self.data):
            for x, v in enumerate(l):
                if v != 0:
                    continue
                nei = self.get_neighbors(x, y)
                if nei.get_charged() >= 3:
                    self.set_pixel(Pixel(x, y, 1))


    # Работа с семенами
    # =================

    def grow(self, pix: Pixel) -> None:
        """Рассеивает энергию вокрег целевого пикселя.

        Один из примеров использования получения соседних пикселей.
        Беря целевой пиксель как точку основы, мы проходимся по всем
        его сосдея, чтобы заполнить их энергией, на единицу меньше.
        После проходимся уже по новым соседям, чтобы энергия
        распространялась дальше, пока не закончится потенциал.

        :param pix: Начальная точка рассеивания энергии.
        :type pix: Pixel
        """
        if pix.x < 0 or pix.x > self.size-1 or pix.y < 0 or pix.y > self.size-1:
            logger.warning("Seed position abroad")
            return

        # logger.debug("Grow seed {}...", pix)
        targets = [pix]
        self.set_pixel(pix)

        for p in targets:
            if p.value <= 1:
                continue

            nei = self.get_neighbors(p.x, p.y)
            for n in nei:
                if n is None or n.value > p.value:
                    continue
                np = Pixel(n.x, n.y, p.value-1)
                self.set_pixel(np)
                targets.append(np)

    def fade(self, pix: Pixel) -> None:
        """Является обратным действием относительно рассеивания.

        Алгоритм подобен рассеиванию, однако в отличие от рассеивания
        затемнение заполняет целевую област миимальными единицами
        энергии, как бы затеняя её.
        К тому же после затенения остаются до 4-х семечек.

        :param pix: Точка начала затемнения.
        :type pix: Pixel
        :return: Пиксели полученных после затенения семян.
        :rtype: list[Pixel]
        """
        # logger.debug("Fade {}...", pix)
        self.set_pixel(Pixel(pix.x, pix.y, 1))
        targets = [pix]

        for p in targets:
            if p.value <= 1:
                continue

            nei = self.get_neighbors(p.x, p.y)
            for n in nei:
                if n is None or n.value > p.value:
                    continue
                self.set_pixel(Pixel(n.x, n.y, 1))
                targets.append(Pixel(n.x, n.y, p.value-1))


    # Рабоат с шагами рендера
    # =======================

    def grow_seeds(self) -> None:
        """Производит рассеивание всхе семян.

        Более высокоуровневая функция.
        Используется при работе с симуляцией.
        Вщращивает все семечки или же рассеивает их потенциал.
        Если несколько семян расположены близко, то они будут поглащены.
        """
        logger.info("Grow seeds...")
        for seed in self.seeds:
            self.grow(seed)
            self.spirits.append(seed)
            self.seeds.set_pixel(seed.update(0))

    def fade_spirits(self) -> None:
        """Затеняет все семена.

        Более высокоуровневая функция, используется при симуляции.
        Затеняет все живущие орагинизмы, а также высаживает все новые
        семена потомков.
        """
        logger.info("fade {} spirits...", len(self.spirits))
        pb = ProgressBar(len(self.spirits))
        while len(self.spirits):
            spirit = self.spirits.pop(0)
            pb.iter(spirit)
            self.fade(spirit)
            self.seeds.get_seeds(spirit)


# Функции для работы с изображением
# =================================

def get_image(colormap: list[list[Color]]) -> Image:
    logger.info("Export colormap as image")
    array = numpy.array(colormap, dtype=numpy.uint8)
    image = Image.fromarray(array)
    image = image.resize((1024, 1024), resample=Image.Resampling.NEAREST)
    return image


# Главная функция скрипта
# =======================

def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Spirit life game simulation")
    parser.add_argument("-s", "--steps",
        help="Limit steps of simulation", type=int, default=30
    )
    parser.add_argument("-o", "--out", type=str, help="Path to save results",
        default="out/")
    parser.add_argument("-i", "--interact", help="Interactive simulation",
        action="store_true"
    )
    parser.add_argument("-p", "--photos", help="Save all frames",
        action="store_true"
    )
    parser.add_argument("-G", "--no-gif", help="No save gif image",
        action="store_true"
    )
    parser.add_argument("-W", "--no-grow-frames", action="store_true",
        help="Do not save simulation grow frames")
    parser.add_argument("-d", "--duration", help="GIF frame duration",
        type=int, default=600)
    return parser

def main():
    pm = Canvas(size=32, start_seed=Pixel(16, 16, 6))
    parser = get_parser()
    args = parser.parse_args()

    out_path = Path(args.out)
    max_step = args.steps
    step = 0
    frames = []

    logger.info("Grow first seed")
    pm.grow_seeds()

    out_path.mkdir(parents=True, exist_ok=True)

    if not args.no_gif:
        frames.append(get_image(pm.colorize(palette)))
    if args.photos:
        get_image(pm.colorize(palette)).save(f'out/0b.png')
    if args.interact:
        get_image(pm.colorize(palette)).show()
        input("-- pause | Press enter --")

    # Run generate steps
    while step < max_step:
        step += 1
        logger.info("Generate step {} of {}", step, max_step)
        pm.fade_spirits()

        if not args.no_gif and not args.no_grow_frames:
            frames.append(get_image(pm.seeds.colorize(palette)))
        if args.photos:
            get_image(pm.seeds.colorize(palette)).save(f'out/{step}a.png')
        if args.interact:
            get_image(pm.seeds.colorize(palette)).show()
            input("-- pause | Press enter --")

        if len(pm.seeds) == 0:
            break

        pm.grow_seeds()

        logger.info("Normalize pixmap")
        pm.normalize()

        if not args.no_gif:
            frames.append(get_image(pm.colorize(palette)))
        if args.photos:
            get_image(pm.colorize(palette)).save(f'out/{step}b.png')
        if args.interact:
            get_image(pm.colorize(palette)).show()
            input("-- pause | Press enter --")

    if not args.no_gif:
        logger.info("Export {} frames to gif in out/out.gif", len(frames))
        frames[0].save(
            "out/out.gif", save_all=True, append_images=frames[1:],
            duration=args.duration, loop=0
        )


# Запуск скрипта
# ==============

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.warning("Stop simulation")
        exit(0)
